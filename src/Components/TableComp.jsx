import React from 'react'
export default function TableComp({data}) {
  return (
      <div>
          <div class="relative overflow-x-auto">
              <table class="w-2/3 text-sm text-left mx-auto text-gray-500 dark:text-gray-400">
                  <thead class="text-xs text-gray-900 uppercase dark:text-gray-400 bg-gray-100">
                      <tr>
                          <th scope="col" class="px-6 py-3 font-bold text-red-700">
                              id
                          </th>
                          <th scope="col" class="px-6 py-3 font-bold text-red-700">
                              email
                          </th>
                          <th scope="col" class="px-6 py-3 font-bold text-red-700">
                              Username
                          </th>
                          <th scope="col" class="px-6 py-3 font-bold text-red-700">
                              age
                          </th>
                      </tr>
                  </thead>
                  <tbody>
                    {data.map((item)=>(
                        <tr key={item.id} class="bg-white dark:bg-gray-800  border-1">
                        <td class="px-6 py-4 font-medium text-gray-900">
                            {item.id}
                        </td>
                        <td class="px-6 py-4 px-6 py-4 font-medium text-gray-900">
                            {item.email}
                        </td>
                        <td class="px-6 py-4 px-6 py-4 font-medium text-gray-900">
                            {item.userName}
                        </td>
                        <td class="px-6 py-4 px-6 py-4 font-medium text-gray-900">
                            {item.age}
                        </td>
                    </tr>
                    ))};
                  </tbody>
              </table>
          </div>
      </div>
  )
}

