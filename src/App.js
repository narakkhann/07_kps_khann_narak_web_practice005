import logo from './logo.svg';
import './App.css';
import InputComp from './Components/InputComp';
import { useState } from 'react';
import "flowbite";

function App() {
  const [person, setPerson] = useState([
    {
      id: 1,
      email: "narakkhann@gmail.com",
      userName: "Khann Narak",
      age: 21,
    },
  ]);
  return (
    <div className="App bg-green-200 h-screen">
      <InputComp person={person} setPerson={setPerson}/>
    </div>
  );
}

export default App;
